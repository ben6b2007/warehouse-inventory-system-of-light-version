# Warehouse Inventory System of Light version

## System Structure

* Backend: Java Spring Boot
* Frontend: React.js
* Database: MariaDB

## Environment
* Java version: 11
* Node version: 12.18.4
* Npm version: 6.14.6 
* Yarn version: 1.22.4
* MariaDB version: 10.1.28

## Start Java Spring Boot
* Run 'src\main\java\com\example\demo\DemoApplication.java'
* The service would be start
## Start React.js
* cd src\main\resources\templates\frontend
* npm install <-- Download node_module
* yarn start
* The website would be shown

## Start MariaDB
* cd db
* run xampp_start.exe
* The db would be run

## Frontend description
![image](frontend.jpg)
* The website can search by product code
* The number of the showing row would be shown as last column of Product ID
* The last column of Product Quantities is showing the total value of quantities
* If double click the cell, the value can be edited, after that, if click modify, the value would be changed
* If click delete button, the record would be deleted
* If upload the csv, the csv data would be upload to db
* Click Download CSV, it will be download all data from db via csv format

### CSV format
<p>The csv format should following as below:
</p>
<p>name,code,weight,location,quantities,productId</p>
<p>TV,TV01,99.99,Tko,1,77</p>

## Review
<p>This is my first time to develop a Java Spring boot & React for whole system. Therefore, I spent much more time to learn Java Spring Boot & React.js </p>
<p>In the first day, I designed the database & table structure in SQL. Besides that,I learn the basic strcture of the Java Spring Boot, such as pom.xml(dependencies), connect db, CRUD , MVC, etc. </p>
<p>In the second day, I designed a basic layout of the frontend and learn how's react.js work</p>
<p>If I can do it again, I should take more time to consider the DB & table structure. If I create a master table for storage product data, a location table for storage shop info. and inventory table for storage of the record of inventory. It should be better. </p>
<p>However, this is the first time that I create a Java Spring Boot backend & React.js frontend. Also, the time is so tight, so I cannot do it perfectly.</p>


