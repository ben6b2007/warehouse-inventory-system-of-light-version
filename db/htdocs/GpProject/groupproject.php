<?php
//
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Group Project</title>

    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="./css/bootstrap.min.css" />
    <link rel="stylesheet" href="./css/groupproject.css" />

</head>

<body>
  <header>
    <div class="topHeaderRow">
        <ui class="nav navbar-right nav-pills">
            <li>
                <a href="#"><span class="glyphicon glyphicon-log-in"></span>Logout</a>
            </li>
            <li>
                <a href="#"><span class="glyphicon glyphicon-user"></span>Profile</a>
            </li>
            <li>
                <a href="#"><span class="glyphicon glyphicon-star"></span>Favorities</a>
            </li>
        </ui>
    </div>
  </header>
   <div class="row">
       <div class="secondHeaderRow">
        <nav class="navbar bg-success">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Share Your Travels</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Browse <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right" role="search" action="select_data.php" method="get">
                <div class="form-group">
                <input type="text" class="form-control" placeholder="Search" id="search" name="search">
                </div>
                <button type="submit" class="btn bg-primary">Submit</button>
            </form>
        </nav>
    </div>
   </div>
    

        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>

</html>
                        
