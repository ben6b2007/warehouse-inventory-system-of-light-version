<?php
require_once('db_config.php');

try{
    $db = new Database("localhost","msim3211_project","root","");

    if($_SERVER['REQUEST_METHOD']=="GET"){
        echo json_encode($db->query("SELECT * FROM msim3211_project.ContactUs"));
        http_response_code(200);
    }else if($_SERVER["REQUEST_METHOD"]=="POST"){
        $sql = 'INSERT INTO msim3211_project.ContactUs (Name,Email,Message) VALUES (?,?,?)';
        echo json_encode($db->insert($sql,$_POST["Name"],$_POST["Email"],$_POST["Message"]));
        http_response_code(200);
    }else{
        http_response_code(405);
    }
}catch(PDOException $e){
    echo $e;
}


?>