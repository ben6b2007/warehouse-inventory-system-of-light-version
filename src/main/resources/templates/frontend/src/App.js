import './App.css';
import React, { Component, useState, Button, Modal} from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'; 
import BootstrapTable, { ROW_SELECT_SINGLE } from 'react-bootstrap-table-next';
import axios from 'axios';
import cellEditFactory from 'react-bootstrap-table2-editor';
import { Type } from 'react-bootstrap-table2-editor';
import paginationFactory from "react-bootstrap-table2-paginator";
import {CSVLink, CSVDownload} from 'react-csv';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';  
import CSVReader from "react-csv-reader";
/*const data = [
  {id: 1, name: 'Gob', code: '2','weight':99.9,'location':'TKO'},
  {id: 2, name: 'Buster', code: '5','weight':99.9,'location':'TKO'},
  {id: 3, name: 'George Michael', code: '4', 'weight':99.9,'location':'TKO'}
];*/


const api = axios.create({
  baseURL: 'http://localhost:8080',
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    }
})


const columns = [{
  dataField: 'productId',
  text: 'Product ID',
  sort:true,
  editor: {
    type: Type.TEXT
  },
  footerTitle: true,
  footer: (columnData, column, columnIndex) => columnData.reduce((total,item) =>  total+=1, 0)
  
}, {
  dataField: 'name',
  text: 'Product Name',
  sort:true,
  editor: {
    type: Type.TEXT
  },
  footer: ''
  
}, {
  dataField: 'code',
  text: 'Product Code',
  sort:true,
  editor: {
    type: Type.TEXT
  },
  filter: textFilter(), 
  footer: ''
},
{
  dataField: 'weight',
  text: 'Product weight',
  sort:true,
  editor: {
    type: Type.TEXT
  },
  footer: ''
},
{
  dataField: 'location',
  text: 'Product location',
  sort:true,
  editor: {
    type: Type.TEXT
  },
  footer: ''
},
{
    dataField: 'quantities',
    text: 'Product Quantities',
    sort:true,
    editor: {
      type: Type.TEXT
    },
    footer: (columnData, column, columnIndex) => columnData.reduce((acc,item) =>  acc+item, 0)
},
{
  dataField: 'Modify',
  text: 'Modify',
  formatter: (rowContent, row) => {
    return (    
      <button type="button" class="btn btn-primary" onClick = { async () => {
        //var data = row.find(({ productId }) => productId === this.state.data.productId)
        await api.put(`/Product/${row.productId}`,row);
        console.log(row.productId);
        //window.location.reload();
      }} >Modify</button>
    )
  },editable:false,
  csvExport:false
  
},
{
  dataField: 'Delete',
  text: 'Delete',
  formatter: (rowContent, row) => {
    return (  
      <button id="delBtn" type="button" class="btn btn-danger" onClick={ async () => {
        await api.delete('/Product/'+row.productId)
        window.location.reload();
      }}>Delete</button>
    )
  },editable:false,
  csvExport:false
}
];
class App extends Component {
  state = { data: null };

  async componentDidMount() {
    const { data } = await api.get('/Product_List')
    this.setState({ data });
  }

  render() {
    if (!this.state.data) return null;
    console.log(this.state.data);
    const handleForce = function(data, fileInfo){
      data.forEach(async function(d){
        await api.post('/New_Product',d);
        console.log(d);
      })
      alert('data is uploaded!');
      window.location.reload();
    }
     
    const papaparseOptions = {
      header: true,
      dynamicTyping: true,
      skipEmptyLines: true,
      transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
    };

    return (
      <div className="App">
        
        <h1 className="Table-header col-sm-12 btn btn-info">Light version Warehouse Inventory System</h1>
        <BootstrapTable striped hover 
          pagination={paginationFactory({ page: 1,
            sizePerPage: 5,
            nextPageText: '>',
            prePageText: '<',
            showTotal: true })}
                  cellEdit={ cellEditFactory({ mode: 'dbclick', blurToSave: true,
                  afterSaveCell: async(oldValue, newValue, row, column) => {
                      //var data = row.find(({ productId }) => productId === this.state.data.productId)
                      var dataF = column.dataField;
                      const data = this.state.data.map(products => {
                        // pass the updated row instead of original row in state
                        if(products.productId === row.productId) {
                          row[dataF] = newValue;
                          return row;
                        }
                        return products;
                      })
                      this.setState({data: data});
                      //window.location.reload();
                  }
                  })}

                  filter={ filterFactory() } 
                  keyField='id'  data={ this.state.data } columns ={ columns}  >            
                </BootstrapTable>
                
        <CSVReader
          cssClass="react-csv-input"
          label="Select CSV and upload to db"
          onFileLoaded={handleForce}
          parserOptions={papaparseOptions}
        />
        <p></p>
        <div class="btn btn-outline-dark btn-block"><CSVLink data={this.state.data} >Download CSV</CSVLink></div>

        
      </div>
    );
  }
}
//
/**/
export default App;
