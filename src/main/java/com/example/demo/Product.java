package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "Product")
public class Product {

    private @Id @GeneratedValue Long id;
    private String name;
    private String code;
    private double weight;
    private String location;
    private int quantities;
    Product() {}
    public Product(
                  String name,
                  String code,
                  double weight,
                  String location,
                  int quantities
                  ) {
        this.name = name;
        this.code = code;
        this.weight = weight;
        this.location = location;
        this.quantities = quantities;
    }

    public Long getProductId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
      }    

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public double getWeight() {
        return weight;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public String getLocation() {
        return location;
    }
    public void setQuantities(int quantities){
        this.quantities = quantities;
    }
    public int getQuantities(){
        return quantities;
    }
}
