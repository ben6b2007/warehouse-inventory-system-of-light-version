package com.example.demo;
 
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;  
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
public class HomeController {
    private final ProductRepository repository;

    HomeController(ProductRepository repository) {
        this.repository = repository;
    }

		Product Product;
    
    @GetMapping("/Product_List")
    @ResponseBody
    public List<Product> findall() {
      Iterable<Product> iterable = repository.findAll();
      List<Product> array = StreamSupport
      .stream(iterable.spliterator(), false)
      .collect(Collectors.toList());
        //return repository.findAll();
        return array;
    }

    @PostMapping("/New_Product")
    Product newPlayer(@RequestBody Product newProduct) {
      repository.save(newProduct);
      return newProduct;
    }

    @PutMapping("/Product/{id}")
    Product replacePlayer(@RequestBody Product newProduct, @PathVariable Long id) {
      
      return repository.findById(id)
        .map(Product -> {
          Product.setName(newProduct.getName());
          Product.setCode(newProduct.getCode());
          Product.setWeight(newProduct.getWeight());
          Product.setLocation(newProduct.getLocation());
          Product.setQuantities(newProduct.getQuantities());
          return repository.save(Product);
        })
        .orElseGet(() -> {
          newProduct.setId(id);
          return repository.save(newProduct);
        });
    }
    @DeleteMapping("/Product/{id}")  
    private void deleteBook(@PathVariable Long id) {  
      repository.deleteById(id);  
    }; 

}
